package com.example.myvolumecontrolapp;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView musicText;
    private TextView callText;
    private TextView setCallText;
    private AudioManager audioManager;
    private static final Integer music = AudioManager.STREAM_MUSIC; //STREAM_VOICE_CALL
    private static final Integer call = AudioManager.STREAM_VOICE_CALL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        int volMax = audioManager.getStreamMaxVolume(music);

        musicText = findViewById(R.id.musicView);
        callText = findViewById(R.id.callView);
        setCallText = findViewById(R.id.setCallView);
        seekBar = findViewById(R.id.seekBar);
        seekBar.setMax(volMax);
        UpdateSeekbarTextView("Initialized");
    }

    public void UpdateSeekbarTextView(String toastText) {
        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
        Integer musicVol = audioManager.getStreamVolume(music);
        seekBar.setProgress(musicVol); //Update the seekbar after the change has been made
        musicText.setText(String.valueOf(musicVol));

        int callVol = audioManager.getStreamVolume(call);
        callText.setText(String.valueOf(callVol));
    }

    public void UpButton(View view) {
        audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
        UpdateSeekbarTextView("Volume Up");
    }

    public void DownButton(View view) {
        audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
        UpdateSeekbarTextView("Volume Down");
    }

    public void SetButton(View view) {
        audioManager.setStreamVolume(call,Integer.parseInt(setCallText.getText().toString()),0);
        UpdateSeekbarTextView("Volume Down");
    }


}